package fuelconsumption.controller;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fuelconsumption.model.Receipt;
import fuelconsumption.service.ReceiptService;
import fuelconsumption.to.MonthlyTotalCostReport;

@RestController
@RequestMapping("/receipt")
public class ReceiptRestController {

	@Autowired
	private ReceiptService receiptService;
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> addConsumptionReceipt(@RequestBody Receipt receipt) {
		receiptService.addReceipt(receipt);
		return new ResponseEntity<String>("OK", HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Collection<Receipt> getReceipts() {
		return receiptService.getReceipts();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/type/{typeCode}")
	@ResponseBody
	public Collection<Receipt> getReceiptsByType(@PathVariable String typeCode) {
		return receiptService.getReceiptsByType(typeCode);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/from/{dateFrom}/to/{dateTo}")
	@ResponseBody
	public Collection<Receipt> getRceiptsForPeriod(@PathVariable String dateFrom, @PathVariable String dateTo) {
		return receiptService.getRceiptsForPeriod(dateFrom, dateTo);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/monthly/from/{dateFrom}/to/{dateTo}")
	@ResponseBody
	public Map<String, Collection<Receipt>> getRceiptsByMonth(@PathVariable String dateFrom, @PathVariable String dateTo) {
		return receiptService.getReceiptsGroupedByMonth(dateFrom, dateTo);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/month/total/from/{dateFrom}/to/{dateTo}")
	@ResponseBody
	public Collection<MonthlyTotalCostReport> getMonthlyTotals(@PathVariable String dateFrom, @PathVariable String dateTo) {
		return receiptService.getMonthlyTotals(dateFrom, dateTo);
	}
	
}
