package fuelconsumption.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fuelconsumption.model.Driver;

@Repository
public interface DriverRepository extends JpaRepository<Driver, String> {
	
}
