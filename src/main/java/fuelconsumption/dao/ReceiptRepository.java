package fuelconsumption.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fuelconsumption.model.Receipt;

@Repository
public interface ReceiptRepository extends JpaRepository<Receipt, Long> {

	public List<Receipt> findByTypeCode(String typeCode);
	
	@Query(value = "SELECT * FROM receipt WHERE date >= :date_from AND date < :date_to", nativeQuery = true)
	public List<Receipt> findByPeriod(@Param("date_from") String dateFrom, @Param("date_to") String dateTo);
}
