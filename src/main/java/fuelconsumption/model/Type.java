package fuelconsumption.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Type {

	@Id
	private String code;

	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String typeCode) {
		this.code = typeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
