package fuelconsumption.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fuelconsumption.dao.ReceiptRepository;
import fuelconsumption.model.Receipt;
import fuelconsumption.to.MonthlyTotalCostReport;

@Service
public class ReceiptService {

	@Autowired
	private ReceiptRepository receiptRepository;

	public void addReceipt(Receipt receipt) {
		receipt.setTotalPrice(receipt.getUnitPrice() * receipt.getVolume());
		receiptRepository.save(receipt);
	}

	public Collection<Receipt> getReceipts() {
		return receiptRepository.findAll();
	}

	public Collection<Receipt> getReceiptsByType(String typeCode) {
		return receiptRepository.findByTypeCode(typeCode);
	}

	public Collection<Receipt> getRceiptsForPeriod(String dateFrom, String dateTo) {
		return this.receiptRepository.findByPeriod(dateFrom, dateTo);
	}

	public Map<String, Collection<Receipt>> getReceiptsGroupedByMonth(String dateFrom, String dateTo) {
		Map<String, Collection<Receipt>> result = new HashMap<>();

		for (Receipt receipt : this.receiptRepository.findByPeriod(dateFrom, dateTo)) {
			String yearAndMonth = receipt.getYearAndMonth();
			if (!result.containsKey(yearAndMonth)) {
				result.put(yearAndMonth, new ArrayList<>());
			}
			result.get(yearAndMonth).add(receipt);
		}

		return result;
	}

	public Collection<MonthlyTotalCostReport> getMonthlyTotals(String dateFrom, String dateTo) {
		Collection<MonthlyTotalCostReport> monthlyTotals = new ArrayList<>();
		Map<String, Collection<Receipt>> receiptsByMonth = this.getReceiptsGroupedByMonth(dateFrom, dateTo);

		for (String yearMonth : receiptsByMonth.keySet()) {
			double monthlyTotalPrice = calculateTotalPrice(receiptsByMonth.get(yearMonth));
			monthlyTotals.add(new MonthlyTotalCostReport(yearMonth, monthlyTotalPrice));
		}
		
		return monthlyTotals;
	}

	protected double calculateTotalPrice(Collection<Receipt> receipts) {
		double monthlyTotalPrice = 0;
		for (Receipt receipt : receipts) {
			monthlyTotalPrice += receipt.getTotalPrice();
		}
		return monthlyTotalPrice;
	}
}
