package fuelconsumption.to;

public class MonthlyComplexReport extends MonthlyTotalCostReport {

	private String typeCode;
	private double totalVolume;
	private double averagePrice;
	
	public MonthlyComplexReport(String yearMonth, double totalCost) {
		super(yearMonth, totalCost);
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public double getAveragePrice() {
		return averagePrice;
	}

	public void setAveragePrice(double averageVolume) {
		this.averagePrice = averageVolume;
	}
	
}
