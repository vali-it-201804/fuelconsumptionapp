package fuelconsumption.to;

public class MonthlyTotalCostReport {

	private String yearMonth;
	private double totalCost;

	public MonthlyTotalCostReport(String yearMonth, double totalCost) {
		this.yearMonth = yearMonth;
		this.totalCost = totalCost;
	}

	public String getYearMonth() {
		return yearMonth;
	}

	public double getTotalCost() {
		return totalCost;
	}

}
