INSERT INTO type (code, description) VALUES ('D', 'Diesel');
INSERT INTO type (code, description) VALUES ('95', 'Gas 95');
INSERT INTO type (code, description) VALUES ('98', 'Gas 98');

INSERT INTO driver (code, first_name, last_name, email) VALUES ('MAR', 'Marek', 'Lints', 'marek.lints@gmail.com');
INSERT INTO driver (code, first_name, last_name, email) VALUES ('REI', 'Rein', 'Kask', 'rein.kask@gmail.com');

INSERT INTO receipt (type_code, date, unit_price, volume, total_price, driver_code) VALUES ('D', '2018-03-02', 1.3, 10.0, 13.0, 'MAR');
INSERT INTO receipt (type_code, date, unit_price, volume, total_price, driver_code) VALUES ('95', '2018-03-02', 2.0, 20.0, 40.0, 'REI');
